package cmd

import (
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"

	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/frontend/application"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/frontend/config"
)

var serveOpts struct { //nolint:gochecknoglobals // this is the recommended way to use cobra
	ConfigPath    string
	FSCConfigPath string
}

func init() { //nolint:gochecknoinits,gocyclo // this is the recommended way to use cobra
	serveCommand.Flags().StringVarP(&serveOpts.ConfigPath, "config-path", "", "", "Location of the config")
	serveCommand.Flags().StringVarP(&serveOpts.FSCConfigPath, "fsc-config-path", "", "", "Location of the FSC config")
}

var serveCommand = &cobra.Command{ //nolint:gochecknoglobals // this is the recommended way to use cobra
	Use:   "serve",
	Short: "Start the service",
	Run: func(cmd *cobra.Command, args []string) {
		// Load the global config and organization config
		cfg, err := config.New("../config/global.yaml", serveOpts.ConfigPath, serveOpts.FSCConfigPath)
		if err != nil {
			log.Fatal(fmt.Errorf("failed to load config: %w", err))
		}

		app := application.New(&cfg)

		if err := app.Listen(); err != nil {
			log.Println("error starting server:", err)
			return
		}

		os.Exit(0)
	},
}

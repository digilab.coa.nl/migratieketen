package application

import (
	"context"
	"fmt"
	"net/http"

	bvv "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-api"
	sigma "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api"
)

func (a *Application) getBvvClient() (client *bvv.ClientWithResponses, err error) {
	fscConfig := a.cfg.FscConfig["fictief-np-bvv-backend"] // Note: the BVV is deployed centrally, within the NP organization

	client, err = bvv.NewClientWithResponses(
		fmt.Sprintf("%s/v0", fscConfig.Endpoint),
		bvv.WithRequestEditorFn(func(ctx context.Context, req *http.Request) error {
			req.Header.Set("Fsc-Grant-Hash", fscConfig.FscGrantHash)
			return nil
		}))

	return
}

func (a *Application) getSigmaClient(sigmaFscEndpointName string) (client *sigma.ClientWithResponses, err error) {
	fscConfig := a.cfg.FscConfig[sigmaFscEndpointName]

	client, err = sigma.NewClientWithResponses(
		fmt.Sprintf("%s/v0", fscConfig.Endpoint),
		sigma.WithRequestEditorFn(func(ctx context.Context, req *http.Request) error {
			req.Header.Set("Fsc-Grant-Hash", fscConfig.FscGrantHash)
			return nil
		}))

	return
}

package cmd

import (
	"fmt"
	"log/slog"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-backend/application"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-backend/config"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-backend/pkg/storage"
)

var serveOpts struct { //nolint:gochecknoglobals // this is the recommended way to use cobra
	ConfigPath string
}

func init() { //nolint:gochecknoinits,gocyclo // this is the recommended way to use cobra
	viper.AutomaticEnv()
	flags := serveCommand.Flags()

	flags.StringVarP(&serveOpts.ConfigPath, "mk-config-path", "", "./config/config.yaml", "Location of the config")
	viper.BindPFlag("mk-config_path", flags.Lookup("config-path"))
}

var serveCommand = &cobra.Command{ //nolint:gochecknoglobals // this is the recommended way to use cobra
	Use:   "serve",
	Short: "Start the api",
	Run: func(cmd *cobra.Command, args []string) {
		cfg, err := config.New(serveOpts.ConfigPath)
		if err != nil {
			slog.Error("config new failed", "err", err)
			return
		}
		var logLevel slog.Level
		if cfg.Debug {
			logLevel = slog.LevelDebug
		} else {
			logLevel = slog.LevelInfo
		}
		logger := slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{
			Level: logLevel,
		})).With("application", "http_server")
		logger.Info("Starting fictief bvv backend", "config", cfg, "loglevel", logLevel)

		if err := migrateInit(cfg.PostgresDSN); err != nil {
			logger.Error("migrate init failed", "err", fmt.Errorf("migrate init: %w", err))
			return
		}

		if err := storage.PostgresPerformMigrations(cfg.PostgresDSN, PSQLSchemaName); err != nil {
			logger.Error("failed to migrate db", "err", err)
			return
		}

		db, err := storage.New(cfg.PostgresDSN)
		if err != nil {
			logger.Error("failed to connect to the database", "err", err)
			return
		}

		app := application.New(logger, cfg, db)

		app.Router()

		if err := app.ListenAndServe(); err != nil {
			logger.Error("listen and serve failed", "err", err)
			return
		}

		os.Exit(0)
	},
}
